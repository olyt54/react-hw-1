import React, {Component} from 'react';
import Button from "./components/Button/Button";
import './App.scss';
import Modal from "./components/Modal/Modal";

class App extends Component {
    state = {
        firstModal: {
            isOpen: false,
            isClosed: true
        },
        secondModal: {
            isOpen: false,
            isClosed: true
        }
    }

    openFirstModal = () => {
        this.setState({
            firstModal: {
                isOpen: true,
                isClosed: false
            }
        })
    }

    openSecondModal = () => {
        this.setState({
            secondModal: {
                isOpen: true,
                isClosed: false
            }
        })
    }

    closeFirstModal = () => {
        this.setState({
            firstModal: {
                isOpen: false,
                isClosed: true
            }
        })
    }

    closeSecondModal = () => {
        this.setState({
            secondModal: {
                isOpen: false,
                isClosed: true
            }
        })
    }

    chooseModal() {
        let modal;
        if (this.state.firstModal.isOpen) {
            modal = <Modal
                header={"Тест на стражника Вайтрана"}
                text={"Что Вы делаете, когда Вас манит дорога приключений?"}
                closeButton={true}
                handleClose={this.closeFirstModal}
                actions={{
                    firstBtn: <Button text={"Стрела в колене"} bgColor={"rgba(0, 0, 0, .5)"} handleBtn={this.closeFirstModal}/>,
                    secondBtn: <Button text={"Fusrodah"} bgColor={"rgba(0, 0, 0, .5)"} handleBtn={this.closeFirstModal}/>
                }}
            />
        } else if (this.state.secondModal.isOpen) {
            modal = <Modal
                header={"Message"}
                text={"Теперь вы одеты, как черт"}
                closeButton={true}
                handleClose={this.closeSecondModal}
                actions={{
                    firstBtn: <Button text={"OK"} bgColor={"rgba(0, 0, 0, .5)"} handleBtn={this.closeFirstModal}/>,
                    secondBtn: <Button text={"OK"} bgColor={"rgba(0, 0, 0, .5)"} handleBtn={this.closeFirstModal}/>
                }}
            />
        }
        return modal;
    }

    render() {
        return (
            <div className="App">
                <div className="btn-container">
                  <Button text={"Open first modal"} bgColor={"#606060"} handleBtn={this.openFirstModal}/>
                  <Button text={"Open second modal"} bgColor={"#ffb641"} handleBtn={this.openSecondModal}/>
                </div>
                {this.chooseModal()}
            </div>
        );
    }
}


export default App;
