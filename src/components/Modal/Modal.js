import React, {Component} from 'react';
import Button from "../Button/Button";
import "./modal.scss"

class Modal extends Component {
    render() {
        let closeBtn;
        if (this.props.closeButton) {
            closeBtn = <Button text="&times;" bgColor="transparent" handleBtn={this.props.handleClose}/>
        }

        return (
            <div className="modal-bg" onClick={() => this.props.handleClose()}>
                <div className="modal">
                    <div className="modal-head">
                        <h3 className="modal-head-text">{this.props.header}</h3>
                        {closeBtn}
                    </div>
                    <div className="modal-body">
                        <p className="modal-body-text">{this.props.text}</p>
                        <div className="modal-actions">
                            {this.props.actions.firstBtn}
                            {this.props.actions.secondBtn}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;